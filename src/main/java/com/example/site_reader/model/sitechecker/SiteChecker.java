package com.example.site_reader.model.sitechecker;


import java.util.List;
import java.util.Map;


public interface SiteChecker {
    Map<String, Integer> checkSites(List<String> siteURLs);
}
