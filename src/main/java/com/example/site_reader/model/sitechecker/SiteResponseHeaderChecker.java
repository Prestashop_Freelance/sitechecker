package com.example.site_reader.model.sitechecker;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Primary;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
@Primary
public class SiteResponseHeaderChecker extends AbstractSiteChecker {

    public static final String PRESTA_SHOP_HEADER_VALUE = "PrestaShop";

    private static final Logger logger = LogManager.getLogger(SiteResponseHeaderChecker.class);

    @Override
    protected boolean isPrestaShopSite(String url) {
        if (url == null) {
            return false;
        }

        logger.info("checking site " + url);

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> response = getStringResponseEntity(url);

        if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
            return false;
        }

        List<String> poweredByHeaderValues = response.getHeaders().get("powered-by");

        return poweredByHeaderValues != null && poweredByHeaderValues.contains(PRESTA_SHOP_HEADER_VALUE);
    }

    private ResponseEntity<String> getStringResponseEntity(String url) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        try {
            response = restTemplate.exchange(url, HttpMethod.GET,entity,String.class);
        } catch (RestClientException ex) {
            logger.info("site " + url + " cant be reached");
            logger.error(ex);
        }

        return response;
    }
}
