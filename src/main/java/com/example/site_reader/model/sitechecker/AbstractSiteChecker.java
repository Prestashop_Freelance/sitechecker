package com.example.site_reader.model.sitechecker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractSiteChecker implements SiteChecker {

    @Override
    public Map<String, Integer> checkSites(List<String> siteURLs) {
        Map<String, Integer> checkedSites = new HashMap<>();

        for (int index = 0; index < siteURLs.size(); index++) {

            String url = siteURLs.get(index);
            if (isPrestaShopSite(url)) {
                String domain = getDomain(url);
                checkedSites.put(domain, index);
            }

        }
        return checkedSites;
    }

    protected abstract boolean isPrestaShopSite(String url);

    protected String getDomain(String url) {

        if (url.contains("//")) {
            return url.substring(url.indexOf("//") + 2);
        }
        return url;
    }

}
